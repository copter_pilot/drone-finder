# -*- coding: UTF-8 -*-
__author__ = 'Semyon. D. Vaskov <vaskovsyoma@yandex.ru>'

from django.test import TestCase

# Create your tests here.

from DroneFinder.models import Manufacturers

class ManufacturersModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Manufacturers.objects.create(name='DJI', description='Китайская частная компания, производитель мультикоптеров')