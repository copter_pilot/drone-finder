__author__ = 'Semyon. D. Vaskov <vaskovsyoma@yandex.ru>'

from django.http import *
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login
def page_index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render(request=request))

def page_login(request):
    template = loader.get_template('login.html')
    return HttpResponse(template.render())

def proc_login(request):
    user_name=request.GET['user_name']
    password=request.GET['password']
    user=authenticate(username=user_name, password=password)
    if user is not None:
        if user.is_active:
            #Пользователь полностью авторизован и валиден, ему можно зайти на наш сайт.
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/login/')
    return HttpResponseRedirect('/login/')