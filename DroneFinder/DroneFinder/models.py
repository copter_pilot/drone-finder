# -*- coding: UTF-8 -*-
__author__ = 'Semyon. D. Vaskov <vaskovsyoma@yandex.ru>'

from django.db import models

class Manufacturers(models.Model):
    """
    Производители
    """
    name=models.CharField(max_length=32, primary_key=True)
    description=models.CharField(max_length=254)

class Stores(models.Model):
    """
    Интернет-магазины
    """
    store_id=models.IntegerField(primary_key=True)
    name=models.CharField(max_length=16)

class Drones(models.Model):
    """
    Дроны
    """
    drone_id=models.IntegerField(primary_key=True)
    name = models.CharField(max_length=56)
    manufacturer=models.ForeignKey(Manufacturers, to_field='name', on_delete=models.CASCADE)
    description=models.CharField(max_length=254)

class Dron_Store(models.Model):
    """
    Промежуточная таблица
    """
    class Meta:
        unique_together=(('drone_id', 'store_id'),)

    drone_id = models.ForeignKey(Drones, to_field='drone_id', on_delete=models.CASCADE)
    store_id = models.ForeignKey(Stores, to_field='store_id', on_delete=models.CASCADE)
    url = models.URLField()
    price = models.DecimalField(..., max_digits = 8, decimal_places = 2)

